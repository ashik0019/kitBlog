<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'Md. Admin',
            'username' => 'admin0019',
            'email' => 'admin@kitblog.com',
            'password' => bcrypt('123456'),
        ]);
        DB::table('users')->insert([
            'role_id' => '2',
            'name' => 'Md. Author',
            'username' => 'author0019',
            'email' => 'author@kitblog.com',
            'password' => bcrypt('123456'),
        ]);
    }
}
